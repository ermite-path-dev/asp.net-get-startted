using Microsoft.EntityFrameworkCore;
namespace Pizzeria.Models;
using System.ComponentModel.DataAnnotations;

public class PizzaAdb
{
    [Key]
    public int IdAdb { get; set; }
    public string? NomAdb    { get; set; }
    public string? DescriptionAdb     { get; set; }
    
}

