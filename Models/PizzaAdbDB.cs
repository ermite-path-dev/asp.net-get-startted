using Microsoft.EntityFrameworkCore;

namespace Pizzeria.Models
{
    public class PizzaAdbDB : DbContext
    {
        public PizzaAdbDB(DbContextOptions<PizzaAdbDB> options) : base(options) { }

        public DbSet<PizzaAdb> Pizzas { get; set; } = null!;
    }
}
