using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Pizzeria.Models;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("Pizzas") ?? "Data Source=Pizzas.db";

// Ajouter le service de DbContext
/*builder.Services.AddDbContext<PizzaAdbDB>(options =>
    options.UseInMemoryDatabase("items"));*/
builder.Services.AddSqlite<PizzaAdbDB>(connectionString);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "API Pizzeria",
        Description = "Faire les pizzas que vous aimez",
        Version = "v1"
    });
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pizzeria API V1");
});

app.MapGet("/", () => "Transmission de Donn�es et S�curit� de l'Information");


app.MapGet("/pizzas", async (PizzaAdbDB db) => await db.Pizzas.ToListAsync());


app.MapPost("/pizzas", async (PizzaAdbDB db, PizzaAdb newPizza) =>
{
    await db.Pizzas.AddAsync(newPizza);
    await db.SaveChangesAsync();
    return Results.Created($"/pizzas/{newPizza.IdAdb}", newPizza);
});

app.MapGet("/pizza/{id}", async (PizzaAdbDB db, int id) => await db.Pizzas.FindAsync(id));

app.MapPut("/pizza/{id}", async (PizzaAdbDB db, PizzaAdb updatePizza, int id) =>
{
    var pizza = await db.Pizzas.FindAsync(id);
    if (pizza is null) return Results.NotFound();
    pizza.NomAdb = updatePizza.NomAdb;
    pizza.DescriptionAdb = updatePizza.DescriptionAdb;
    await db.SaveChangesAsync();
    return Results.NoContent();
});

app.MapDelete("/pizza/{id}", async (PizzaAdbDB db, int id) => {
    var pizza = await db.Pizzas.FindAsync(id);
    if (pizza is null) return Results.NotFound();

    db.Pizzas.Remove(pizza);
    await db.SaveChangesAsync(); // Appeler SaveChangesAsync sur le contexte
    return Results.Ok();
});


app.Run();
