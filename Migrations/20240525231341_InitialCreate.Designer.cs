﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Pizzeria.Models;

#nullable disable

namespace Pizzeria.Migrations
{
    [DbContext(typeof(PizzaAdbDB))]
    [Migration("20240525231341_InitialCreate")]
    partial class InitialCreate
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "8.0.5");

            modelBuilder.Entity("Pizzeria.Models.PizzaAdb", b =>
                {
                    b.Property<int>("IdAdb")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("DescriptionAdb")
                        .HasColumnType("TEXT");

                    b.Property<string>("NomAdb")
                        .HasColumnType("TEXT");

                    b.HasKey("IdAdb");

                    b.ToTable("Pizzas");
                });
#pragma warning restore 612, 618
        }
    }
}
